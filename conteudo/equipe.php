<section class="equipe">
    <article class="site">
        <h2>Equipe</h2>
        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit.<br> Ipsum dolor sit amet consectetur.</p>
        <div class="conteudoEquipe">
            <div>
                <img src="img/equipe01.png" alt="Equipe1">
                <h3>Nome</h3>
            </div>
            <div>
                <img src="img/equipe02.png" alt="Equipe2">
                <h3>Nome</h3>
            </div>
            <div>
                <img src="img/equipe03.png" alt="Equipe3">
                <h3>Nome</h3>
            </div>
            <div>
                <img src="img/equipe04.png" alt="Equipe4">
                <h3>Nome</h3>
            </div>
        </div>
    </article>
</section>