<section class="faixaPreta">
    <article class="site topo">
        <h2>(11)999 999 999</h2>
        <ul>
            <!--Lista não ordenada-->
            <li><a href="#"><img src="img/facebooklogo.svg" alt="facebook"></a></li>
            <li><a href="#"><img src="img/instagramlogo.svg" alt="instagram"></a></li>
            <li><a href="#"><img src="img/twitterlogo.svg" alt="twitter"></a></li>
            <li><a href="#"><img src="img/whatsapplogo.svg" alt="whatsapp"></a></li>
        </ul>
    </article>
</section>



<!--MENU-->
<section class="menu">
    <article class="site topo">
        <h1 class="animate__animated animate__rubberBand">Logo Kibeleza</h1>
        <button class="abrirMenu"><!--MENU--></button>
        <nav>
            <button class="fecharMenu"><!--X--></button>
            <ul>
                <a href="index.php" class="item-menu ativo">   <!--Orienta navegação pelas páginas-->
                <li>HOME</li> <!--Link atrelado à forma-->
                </a>
                <a href="sobre.php" class="item-menu">
                <li>SOBRE</li>
                </a>
                <a href="servico.php" class="item-menu">
                <li>SERVIÇO</li>
                </a>
                <a href="news.php" class="item-menu">
                <li>BLOG</li>
                </a>
                <a href="contato.php" class="item-menu">
                <li>CONTATO</li>
                </a>
            </ul>
        </nav>
    </article>
</section>