<section class="info">
<article class="site topo">
    <div class="infoSobre">
        <h3>Sobre</h3>
        <img class="logoRodape" src="img/LogoKiBeleza.svg" alt="logoRodape">
        <p>Lorem, ipsum dolor sit amet consectetur adipisicing elit. Consequuntur temporibus officia
                placeat suscipit nihil in pariatur sit labore consequatur voluptate tempora a, incidunt,
                consectetur dicta ut unde magnam neque cumque.</p>
        <div>
        <ul>
            <li><a href="#"><img src="img/facebooklogo.svg" alt="facebook"></a></li>
            <li><a href="#"><img src="img/instagramlogo.svg" alt="instagram"></a></li>
            <li><a href="#"><img src="img/twitterlogo.svg" alt="instagram"></a></li>
            <li><a href="#"><img src="img/whatsapplogo.svg" alt="instagram"></a></li>
        </ul>
        </div>
    </div>

    <div class="infoLinks">
        <h3>Links</h3>
        <ul>
            <li><a href="#">HOME</a></li> <!--Link atrelado à escrita-->
            <li><a href="#">SOBRE</a></li>
            <li><a href="#">SERVIÇO</a></li>
            <li><a href="#">NEWS</a></li>
            <li><a href="#">CONTATO</a></li>
        </ul>
    </div>

    <div class="infoContato">
        <div>
        <ul>
            <li><a href="#"><img src="img/phone_icon-icons.com_66151.svg" alt="logoTelefone"></a></li>
        </ul>
            <div>
                <p>+55 11 988 626 603</p>
                <p>+55 11 988 626 604</p>
            </div>
        </div>
        <div>
        <ul>
            <li><a href="#"><img src="img/email-3-christmas-icon_icon-icons.com_48846.png" alt="logoEmail"></a></li>
        </ul>
            <p>contato@kibeleza.com</p>
        </div>
        <div>
        <ul>
            <li><a href="#"><img src="img/Citycons_location_icon-icons.com_67931.png" alt="logoEndereco"></a></li>
        </ul>
            <div>
                <p>Av. Marechal Tito, 5000</p>
                <p>São Miguel Paulista - SP</p>
            </div>
        </div>
    </div>
</article>
</section>
<section class="direitos">
<p>© Todos os direitos reservados - <span>TI14 SMP SENAC<span></span></p> <!--alt0169-->
</section>