<!DOCTYPE html>
<html lang="pt-br">

<head>
    <!--Cabeçalho--> <!--Indetação= shft ctl alt f-->
    <meta charset="UTF-8">
    <!--Caracteres especiais-->
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!--Responsivo-->
    <title>KI.Beleza - Clínica de Estética</title>

    <!--Logo da aba-->
    <link rel="icon" href="img/logo.svg">

    <!--Resetar estilos CSS - Sempre primeiro-->
    <link rel="stylesheet" href="css/reset.css">

    <!--Animação do banner-->
    <link rel="stylesheet" type="text/css" href="css/slick.css"/>
    <link rel="stylesheet" type="text/css" href="css/slick-theme.css"/>

    <!--Animação da logo-->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.1.1/animate.min.css"/>

    <!--Animação vid destaque-->
    <link href="css/lity.css" rel="stylesheet">

    <!--Meu estilo CSS - Sempre por último-->
    <link rel="stylesheet" href="css/estilo.css">
</head>



<body>
    <!--INICIO DO TOPO-->
    <header>
        <!--TOPO-->
        <?php require_once("conteudo/topo.php")?>
    </header>
    <!--FIM DO TOPO-->



    <!--INICIO DO CONTEÚDO-->
    <main>

        <?php require_once("conteudo/sobre.php")?>
        
        <?php require_once("conteudo/destaque.php")?>

        <?php require_once("conteudo/galeria.php")?>
        
    </main>
    <!--FIM DO CONTEÚDO-->



    <!--INICIO DO RODAPÉ-->
    <footer>
    <?php require_once("conteudo/rodape.php")?>
    </footer>
    <!--FIM DO RODAPÉ-->



    <!--A animação deve ser carregada por último, senão pode ser comprometida na inicialização da página-->
    <script type="text/javascript" src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
    <script type="text/javascript" src="js/slick.min.js"></script>

    <!--Animação do destaque-->
    <script src="js/lity.js"></script>


    <script type="text/javascript" src="js/minhaAnimacao.js"></script>
</body>
</html>