$(document).ready(function(){
    $('.banner').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        autoplay: true,
        autoplaySpeed: 2000,
        pauseOnHover: false /*Quando falso o mouse não pausa o processo*/
      });
});



/*CARROSSEL GALERIA*/

$(document).ready(function(){
  $('.galeria').slick({
      slidesToShow: 6,
      slidesToScroll: 1,
      autoplay: true,
      autoplaySpeed: 1000,
      pauseOnHover: false,
      arrows: false,
      responsive: [
        {
          breakpoint: 1204,        /*1200*/  /*Slick trabalha com medidas quebradas*/ 
          settings: {
            slidesToShow: 4,
            slidesToScroll: 1,
            autoplay: true,
            autoplaySpeed: 1000,
            pauseOnHover: false,
            arrows: false
          }
        },
        {
          breakpoint: 1000,         /*960*/
          settings: {
            slidesToShow: 3,
            slidesToScroll: 1,
            autoplay: true,
            autoplaySpeed: 1000,
            pauseOnHover: false, 
            arrows: false
          }
        },
        {
          breakpoint: 600, /*600*/
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1,
            autoplay: true,
            autoplaySpeed: 1000,
            pauseOnHover: false,
            arrows: false,
          }
        }
        // You can unslick at a given breakpoint now by adding:
        // settings: "unslick"
        // instead of a settings object
      ]
    });
});



/*BOTÕES MENU, CONF SELEÇÃO*/

$(document).ready(function(){
  $('.item-menu').click(function()
    {
      $('.item-menu').removeClass('ativo');
      $(this).addClass('ativo');
    });
});



/*MENU MOBILE*/
document.querySelector(".abrirMenu").onclick = function(){           /*querySelector: relacionado ao funcionamento do click*/
  document.documentElement.classList.add("menuAtivo");
}
document.querySelector(".fecharMenu").onclick = function(){
  document.documentElement.classList.remove("menuAtivo");
}