<?php
    require_once("conexao.php");

    $conexao = Conexao::LigarConexao();
    $conexao->exec("SET NAMES utf8");

    if(!$conexao){
        echo "Não foi possivel conectar ao banco de dados!";
    }

    if(isset($_GET['idServico'])){

        $idServico = $_GET['idServico'];

        $query = $conexao->prepare("SELECT * FROM `servico` WHERE idServico = $idServico");

        $query->execute();
        $json = array();

        $dados = $query->fetch(PDO::FETCH_ASSOC);
        array_push($json, $dados);

        

        echo json_encode($json, JSON_UNESCAPED_UNICODE);



    }

    //isset=verifica se var foi inicializada ou n
    //se a var idSer for inicializada, o valor atribuido será idServico 
?>