<?php
    require_once("conexao.php");

    header('Content-type: application/json; charset=utf-8');
    header('Access-Control-Allow-Method: POST');

    //var_dump($_POST);

    $data = file_get_contents("php://input");
    $objData = json_decode($data);

    //var_dump($data);
    //var_dump($objData);

    $nome = $objData->nome; //os trÊs são os mesmos do objeto criado em formConst
    $email = $objData->email;
    $senha = $objData->senha; //sem criptografia por enquanto

    $dataCad = date('Y-m-d');
    $status = 'ATIVO';
    $fotoUser = 'cliente/user.png';


    $nome = stripcslashes($nome);
    $email = stripcslashes($email);
    $senha = stripcslashes($senha); //remove barras na declaração


    $nome = trim($nome);  //em javascript, trim faz remoção de espaços no inic e fim da declaração/string
    $email = trim($email);
    //$senha = trim($senha);

    //var_dump($nome); teste de chegada de info no parametro declarado

    $conexao = Conexao::LigarConexao(); //etapa q grava dados e conecta servidor
    $conexao->exec("SET NAMES utf8");

    if($conexao){
        $query = $conexao->prepare("INSERT INTO `cliente`(nomeCliente, emailCliente, senhaCliente, statusCliente, dataCadCliente, fotoCliente) 
                                    VALUES ('".$nome."', '".$email."', '".$senha."', '".$status."', '".$dataCad."', '".$fotoUser."');");
        
        $query->execute();

        $dadosCadastro = array('mens' => 'Dados cadastrados com sucesso.');
        echo json_encode($dadosCadastro);        


    
    }else{
        $dadosCadastro = array('mens' => 'Não foi possível realizar o cadastro.');
        echo json_encode($dadosCadastro);  
    }

?>