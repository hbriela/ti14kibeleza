<?php //msm logica de cadastro
    require_once("conexao.php");

    header('Content-type: application/json; charset=utf-8');
    header('Access-Control-Allow-Method: POST');

    $data = file_get_contents("php://input");
    $objData = json_decode($data); //decodificando

    $codFunc = $objData->codFunc;
    $dataReserva = $objData->dataReserva;
    $codCliente = $objData->codCliente;
    $codServico = $objData->codServico;

    $obsReserva = "Reserva realizada pelo APP";
    $statusReserva = "AGUARDANDO";
    $dataReserva = $dataReserva;
    $horaReserva = $dataReserva;

    $codFunc = stripcslashes($codFunc); //remov barras
    $codCliente = stripcslashes($codCliente);
    $codServico = stripcslashes($codServico);

    $codFunc = trim($codFunc); //remov espaçamento
    $codCliente = trim($codCliente);
    $codServico = trim($codServico);
    
    $conexao = Conexao::LigarConexao();
    $conexao->exec("SET NAMES utf8");

    if($conexao){
        $query = $conexao->prepare("INSERT INTO `reserva`(obsReserva, dataReserva, horaReserva, statusReserva, idFuncionario, idCliente, idServico) 
                                    VALUES ('".$obsReserva."', '".$dataReserva."', '".$horaReserva."', '".$statusReserva."', '".$codFunc."', '".$codCliente."', '".$codServico."');");
        
        $query->execute();

        $dadosCadastro = array('mens' => 'Dados cadastrados com sucesso.');
        echo json_encode($dadosCadastro);        
    
    }else{
        $dadosCadastro = array('mens' => 'Não foi possível realizar o cadastro.');
        echo json_encode($dadosCadastro);  
    }
?>